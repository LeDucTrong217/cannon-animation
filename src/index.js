import keyInput from "./keyinput.js";
const ratio = window.innerWidth / window.innerHeight;

const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(75, ratio, 0.1, 1000);

const renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

const light = new THREE.AmbientLight(0x404040);
const dlight = new THREE.DirectionalLight(0xffffff, 0.5);

light.add(dlight);
scene.add(light);

const geometry = new THREE.BoxGeometry(50, 1, 50);
const material = new THREE.MeshPhongMaterial({ color: 0xffffff });
const ground = new THREE.Mesh(geometry, material);

scene.add(ground);
camera.position.set(-20, 50, 10);

const boxGeometry = new THREE.BoxGeometry(2, 2, 2);
const boxMaterial = new THREE.MeshPhongMaterial({ color: 0x00ff00 });
const box = new THREE.Mesh(boxGeometry, boxMaterial);
box.position.set(-25, 2, 0)
scene.add(box);
let dodecahedronGeometry = new THREE.DodecahedronGeometry(1, 0)
let dodecahedronMaterial = new THREE.MeshPhongMaterial({ color: 0x590696 });
let dodecahedron = new THREE.Mesh(dodecahedronGeometry, dodecahedronMaterial);
dodecahedron.position.set(-25, 1, 0)
scene.add(dodecahedron);
var velocityX = 1;
var velocityZ = 0.05;

function randomVelocity(max, min) {
    return (Math.random() * (max - min) + min).toFixed(3)
}

function animate() {
    requestAnimationFrame(animate);
    if (keyInput.isPressed(87)) {
        // box.position.z -= 0.1;
        camera.position.z -= 0.05;
    }
    if (keyInput.isPressed(83)) {
        // box.position.z += 0.1;
        camera.position.z += 0.05;
    }
    if (keyInput.isPressed(65)) {
        // box.position.x -= 0.1;
        camera.position.x += 0.05;
    }
    if (keyInput.isPressed(68)) {
        // box.position.x += 0.1;
        camera.position.x -= 0.05;
    }
    if (keyInput.isPressed(81)) {
        // box.position.y -= 0.1;
        camera.position.y += 0.05;
    }
    if (keyInput.isPressed(69)) {
        // box.position.y += 0.1;
        camera.position.y -= 0.05;
    }
    if (keyInput.isPressed(32)) {
        status = true;
        velocityX = 0.92463851;
        velocityZ = 0.92463851;
        dodecahedron.position.x = -24;
        dodecahedron.position.z = 0;
    }
    if (status) {
        dodecahedron.rotation.x += 0.5;
        dodecahedron.rotation.y += 0.5;
        dodecahedron.rotation.z += 0.5;

        let attenuation = Math.random() * (4 - 2) + 2;

        if (dodecahedron.position.x > 24 || dodecahedron.position.x < -24) {
            velocityX = -velocityX + velocityX / attenuation;
        }

        if (dodecahedron.position.z > 24 || dodecahedron.position.z < -24) {
            velocityZ = -velocityZ + velocityZ / attenuation;
        }

        dodecahedron.position.x += velocityX;
        dodecahedron.position.z += velocityZ;
        if (velocityX <= 0.001 && velocityX >= -0.001 && velocityZ <= 0.01 && velocityZ >= -0.001) {
            status = false;
            dodecahedron.rotation.x = 0;
            dodecahedron.rotation.y = 0;
            dodecahedron.rotation.z = 0;
        }
    }
    camera.lookAt(ground.position);
    renderer.render(scene, camera);
}
animate();

// Duyệt phần tử theo thuộc tính
// scene.traverse(function(child) {
//     if (child.class === "item") {
//         child.rotation.x += num;
//         child.rotation.y += num;
//     }
// });